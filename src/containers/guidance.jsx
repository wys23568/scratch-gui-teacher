import React from 'react';
import omit from 'lodash.omit';
import Guidance from '../../src/components/guidance/guidance.jsx';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {STAGE_DISPLAY_SIZES} from '../lib/layout-constants';
import {getStageDimensions} from "../lib/screen-utils";

const GuidanceCon = props => {
    const {
        onCloseGuidance,
        guidanceVisible,
        isFullScreen,
        stageSize
    } = omit(props, 'dispatch');
    let width = 0;
    let height = 0;
    let stageDimensions = null;
    if (guidanceVisible) {
        height = document.documentElement.clientHeight;
        width = document.documentElement.clientWidth;
        stageDimensions = getStageDimensions(stageSize, isFullScreen);
        console.log('stageDimensions ', stageSize, stageDimensions);
    }
    return (
        guidanceVisible ? <Guidance
            borderHeight={height}
            borderWidthValue={width}
            closeGuidance={onCloseGuidance}
            stageDimensions={stageDimensions}
        /> : null
    );
};


const mapStateToProps = state => ({
    isFullScreen: state.scratchGui.mode.isFullScreen
});

export default connect(mapStateToProps, null)(GuidanceCon);
