import bindAll from 'lodash.bindall';
import PropTypes from 'prop-types';
import React from 'react';
import {connect} from 'react-redux';
import {projectTitleInitialState} from '../reducers/project-title';
import {changeURLPar, saveProduction} from '../lib/download-blob';
import log from '../lib/log';
import {PAGE_TYPE} from "../lib/gui-constant";
import {homeworkSave} from "../lib/couresHomework";
import {productionUpdate} from "../lib/productionFetch";
import {setProductionId} from "../reducers/mode";
import {message} from 'antd';
import {Canvas2Image} from '../playground/canvas2image';
import html2canvas from '../playground/html2canvas.min.js';
/**
 * 保存作品到服务器
 */
class SB3SaveWork extends React.Component {
    constructor (props) {
        super(props);
        bindAll(this, [
            'downloadProject',
            'exportCanvasAsPNG',
            'dataURLtoBlob'
        ]);
    }

    dataURLtoBlob (dataurl) {
        const arr = dataurl.split(',');
        const mime = arr[0].match(/:(.*?);/)[1];
        const bstr = atob(arr[1]);
        let n = bstr.length;
        const u8arr = new Uint8Array(n);
        while (n--){
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new Blob([u8arr], {type: mime});
    }

    exportCanvasAsPNG (id, imageName, content) {

        setTimeout(() => {
            const canvasElement = document.getElementById(id);
            canvasElement.setAttribute('crossOrigin', 'anonymous');
            const dataUrl = canvasElement.toDataURL('image/jpeg', 1.0);
            const blob = this.dataURLtoBlob(dataUrl);
            log.info('封面大小', blob.size, dataUrl);

            this.props.onOpenLoadingState('作品保存中');
            if (this.props.productionId) {
                productionUpdate(this.props.loginToken, this.props.projectFilename, content,
                    this.props.projectTitle, blob, imageName, this.props.productionId)
                    .then(() => {
                        message.success('保存成功');
                        this.props.onCloseLoadingState();
                    })
                    .catch(() => {
                        message.error('保存失败');
                        this.props.onCloseLoadingState();
                    });
            } else {
                // 上传到服务器
                saveProduction(this.props.loginToken, this.props.projectFilename, content,
                    blob, imageName, this.props.projectTitle)
                    .then(result => {
                        message.success('保存成功');
                        this.props.onSetProductionId(result.id);
                        // 获取当前页面的网址信息
                        let url = document.URL;
                        url = changeURLPar(url, 'productionId', result.id);
                        // 将网址设置
                        history.pushState(null, null, url);
                        this.props.onCloseLoadingState();
                    })
                    .catch(() => {
                        message.error('保存失败');
                        this.props.onCloseLoadingState();
                    });
            }
        }, 100);

        const test = document.getElementById('scratchCanvas');
        html2canvas(test).then(canvas => {
            console.log('封面大小 canvas1', canvas);
        });
    }

    downloadProject () {
        this.props.saveProjectSb3().then(content => {
            if (this.props.onSaveFinished) {
                this.props.onSaveFinished();
            }
            if (this.props.pageType === PAGE_TYPE.createProduction) {

                this.exportCanvasAsPNG('scratchCanvas', 'sb3mao.png', content);

            } else {
                this.props.onOpenLoadingState('作业保存中');
                homeworkSave(this.props.loginToken, this.props.projectFilename, content, this.props.courseId,
                    this.props.courseLessonId, this.props.teacherCourseId)
                    .then(result => {
                        message.success('保存成功');
                        this.props.onCloseLoadingState();
                    })
                    .catch(error => {
                        message.error('保存失败');
                        this.props.onCloseLoadingState();
                    });
            }
        });
    }


    render () {
        const {
            children
        } = this.props;
        return children(
            this.props.className,
            this.downloadProject
        );
    }
}

const getProjectFilename = (curTitle, defaultTitle) => {
    let filenameTitle = curTitle;
    if (!filenameTitle || filenameTitle.length === 0) {
        filenameTitle = defaultTitle;
    }
    return `${filenameTitle.substring(0, 100)}.sb3`;
};

SB3SaveWork.propTypes = {
    children: PropTypes.func,
    className: PropTypes.string,
    courseId: PropTypes.number,
    courseLessonId: PropTypes.number,
    loginToken: PropTypes.string,
    onCloseLoadingState: PropTypes.func,
    onOpenLoadingState: PropTypes.func,
    onSaveFinished: PropTypes.func,
    onSetProductionId: PropTypes.func,
    pageType: PropTypes.number,
    productionId: PropTypes.number,
    projectFilename: PropTypes.string,
    projectTitle: PropTypes.string,
    saveProjectSb3: PropTypes.func,
    teacherCourseId: PropTypes.number
};
SB3SaveWork.defaultProps = {
    className: ''
};

const mapStateToProps = state => ({
    saveProjectSb3: state.scratchGui.vm.saveProjectSb3.bind(state.scratchGui.vm),
    projectFilename: getProjectFilename(state.scratchGui.projectTitle, projectTitleInitialState),
    projectTitle: state.scratchGui.projectTitle,
    pageType: state.scratchGui.mode.pageType,
    productionId: state.scratchGui.mode.productionId,
    courseId: state.scratchGui.mode.courseId,
    courseLessonId: state.scratchGui.mode.courseLessonId,
    teacherCourseId: state.scratchGui.mode.teacherCourseId,
    loginToken: state.scratchGui.mode.loginToken
});

const mapDispatchToProps = dispatch => ({
    onSetProductionId: productionId => dispatch(setProductionId(productionId))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps // omit dispatch prop
)(SB3SaveWork);
