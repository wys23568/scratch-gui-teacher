import React from 'react';
import omit from 'lodash.omit';
import HomeWorkVideo from '../components/Myvideo/HomeWorkVideo.jsx';
import {PAGE_TYPE} from '../lib/gui-constant';

const VideoHomeWork = props => {
    const {
        closeHomeWorkVideoModal,
        openHomeWorkVideoModal,
        homeworkVideoVisible,
        pageType,
        videoUrl
    } = omit(props, 'dispatch');

    return (
        pageType === PAGE_TYPE.createHomeWork && videoUrl ? <HomeWorkVideo
            closeHomeWorkVideoModal={closeHomeWorkVideoModal}
            homeworkVideoVisible={homeworkVideoVisible}
            openHomeWorkVideoModal={openHomeWorkVideoModal}
            videoUrl={videoUrl}
        /> : null
    );
};

export default VideoHomeWork;
