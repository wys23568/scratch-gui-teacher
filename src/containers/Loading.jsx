import React from 'react';
import LoadingText from '../components/loader/loading.jsx';

export default class LoadingCon extends React.Component{
    render () {
        return (
            this.props.loadingTextVisible ? <LoadingText content={this.props.content} /> : null
        );
    }
}
