import ApiUtils from './ApiUtils';
import {saveFile} from './download-blob';

// 查询作业详情
export const messageCommit = function (loginToken, courseId, courseLessonId, teacherCourseId, content) {

    const fileData = new FormData();
    fileData.append('loginToken', loginToken);
    fileData.append('courseId', courseId);
    fileData.append('courseLessonId', courseLessonId);
    fileData.append('teacherCourseId', teacherCourseId);
    fileData.append('content', content);
    return new Promise((resolve, reject) => {
        ApiUtils.post('/account/student/course/homework/message/commit', fileData)
            .then(result => {
                if (result.code === 200) {
                    resolve(result.data);
                } else {
                    reject(result);
                }
            })
            .catch(error => {
                reject(error);
            });
    });
};

// 查询作业详情
export const homeworkDetail = function (loginToken, courseId, courseLessonId, teacherCourseId) {

    const fileData = new FormData();
    fileData.append('loginToken', loginToken);
    fileData.append('courseId', courseId);
    fileData.append('courseLessonId', courseLessonId);
    fileData.append('teacherCourseId', teacherCourseId);
    return new Promise((resolve, reject) => {
        ApiUtils.post('/account/student/course/detail', fileData)
            .then(result => {
                if (result.code === 200) {
                    resolve(result.data);
                } else {
                    reject(result);
                }
            })
            .catch(error => {
                reject(error);
            });
    });
};

// 真实的保存作品
const realHomeworkSave = function (loginToken, courseId, courseLessonId, teacherCourseId, draft) {
    const fileData = new FormData();
    fileData.append('loginToken', loginToken);
    fileData.append('courseId', courseId);
    fileData.append('courseLessonId', courseLessonId);
    fileData.append('teacherCourseId', teacherCourseId);
    fileData.append('draft', draft);

    return new Promise((resolve, reject) => {
        ApiUtils.post('/account/student/course/homework/save', fileData)
            .then(result => {
                if (result.code === 200) {
                    resolve(result);
                } else {
                    reject(result);
                }
            })
            .catch(error => {
                reject(error);
            });
    });
};

// 保存作业
export const homeworkSave = function (loginToken, filename, blob, courseId, courseLessonId, teacherCourseId,) {
    return saveFile(loginToken, filename, blob)
        .then(result => realHomeworkSave(loginToken, courseId, courseLessonId, teacherCourseId, result.data.url));
};


// 真实的保存作品
const realHomeworkCommit = function (loginToken, courseId, courseLessonId, teacherCourseId, content) {
    const fileData = new FormData();
    fileData.append('loginToken', loginToken);
    fileData.append('courseId', courseId);
    fileData.append('courseLessonId', courseLessonId);
    fileData.append('teacherCourseId', teacherCourseId);
    fileData.append('content', content);

    return new Promise((resolve, reject) => {
        ApiUtils.post('/account/student/course/homework/commit', fileData)
            .then(result => {
                if (result.code === 200) {
                    resolve(result);
                } else {
                    reject(result);
                }
            })
            .catch(error => {
                reject(error);
            });
    });
};

// 保存作业
export const homeworkCommit = function (loginToken, filename, blob, courseId, courseLessonId, teacherCourseId,) {
    return saveFile(loginToken, filename, blob)
        .then(result => realHomeworkCommit(loginToken, courseId, courseLessonId, teacherCourseId, result.data.url));
};
