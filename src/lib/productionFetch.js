import ApiUtils from './ApiUtils';
import {saveFile} from './download-blob';

// 查询作品详情
export const productionDetail = function (loginToken, productionId) {

    const fileData = new FormData();
    if (loginToken) {
        fileData.append('loginToken', loginToken);
    }
    fileData.append('productionId', productionId);
    return new Promise((resolve, reject) => {
        ApiUtils.post('/production/detail', fileData)
            .then(result => {
                if (result.code === 200) {
                    resolve(result.data);
                } else {
                    reject(result);
                }
            })
            .catch(error => {
                reject(error);
            });
    });
};

// 真实的更新作品
const realProductionUpdate = function (loginToken, productionId, name, content, coverUrl) {
    const fileData = new FormData();
    fileData.append('loginToken', loginToken);
    fileData.append('productionId', productionId);
    fileData.append('name', name);
    fileData.append('content', content);
    fileData.append('coverUrl', coverUrl);

    return new Promise((resolve, reject) => {
        ApiUtils.post('/account/student/production/update', fileData)
            .then(result => {
                if (result.code === 200) {
                    resolve(result.data);
                } else {
                    reject(result);
                }
            })
            .catch(error => {
                reject(error);
            });
    });
};

// 更新作品
export const productionUpdate = function (loginToken, filename, fileblob, name, imageBlob, imageName, productionId) {
    return saveFile(loginToken, imageName, imageBlob)
        .then(result => saveFile(loginToken, filename, fileblob, result.data.url))
        .then(result => realProductionUpdate(loginToken, productionId, name, result.data.url, result.data.coverUrl));
};
