import ApiUtils from './ApiUtils';

// 获取学生信息
export const studentInfo = function (loginToken) {
    const fileData = new FormData();
    fileData.append('loginToken', loginToken);

    return new Promise((resolve, reject) => {
        ApiUtils.post('/account/student/info', fileData)
            .then(result => {
                if (result.code === 200) {
                    resolve(result);
                } else {
                    reject(result);
                }
            })
            .catch(error => {
                reject(error);
            });
    });
};

