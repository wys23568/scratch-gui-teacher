import React from 'react';
import PropTypes from 'prop-types';
import queryString from 'query-string';
import {connect} from 'react-redux';

import {detectTutorialId} from './tutorial-from-url';

import {activateDeck} from '../reducers/cards';
import {openTipsLibrary} from '../reducers/modals';
import {PAGE_TYPE} from './gui-constant';
import log from '../lib/log';
import {studentInfo} from './UserInfoFetch';
import {
    setLoginToken, setPageType, setPlayer, setProductionId, setProjectUrl,
    setStudentCourseHomeworkId, setWeiXinCode
} from '../reducers/mode';
import {saveUserInfo} from '../reducers/project-state';

/* Higher Order Component to get parameters from the URL query string and initialize redux state
 * @param {React.Component} WrappedComponent: component to render
 * @returns {React.Component} component with query parsing behavior
 */
const QueryParserHOC = function (WrappedComponent) {
    class QueryParserComponent extends React.Component {
        constructor (props) {
            super(props);
            const queryParams = queryString.parse(location.search);
            const tutorialId = detectTutorialId(queryParams);
            if (tutorialId) {
                if (tutorialId === 'all') {
                    this.openTutorials();
                } else {
                    this.setActiveCards(tutorialId);
                }
            }


            // 查询网络值
            this.queryParam.bind(this)();
        }

        // 查询参数
        queryParam (){
            const queryParams = queryString.parse(location.search);
            const type = Number(queryParams.type);
            this.props.onSetPageType(type);
            if (type === PAGE_TYPE.showWork) {
                // 展示作品
                this.props.onSetPlayer(true);
                this.props.onSetProjectUrl(queryParams.url);
                // this.loadProjectFile.bind(this, queryParams.url)();

            } else if (type === PAGE_TYPE.createProduction){
                // 获取loginToken
                const loginToken = this.readLoginToken.bind(this)();
                // 如果有作品id，则保存
                if (queryParams.productionId) {
                    this.props.onSetProductionId(Number(queryParams.productionId));
                }

                if (loginToken) {
                    // 获取个人信息
                    this.loginInfo.bind(this, loginToken)();
                }
            } else if (type === PAGE_TYPE.createHomeWork){
                // 获取loginToken
                const loginToken = this.readLoginToken.bind(this)();
                // 创建课后作业
                const courseId = Number(queryParams.courseId);
                const courseLessonId = Number(queryParams.courseLessonId);
                const teacherCourseId = Number(queryParams.teacherCourseId);
                if (loginToken) {
                    // 获取个人信息
                    this.loginInfo.bind(this, loginToken)();
                    // 编辑作业,获取作业作品，加载
                    this.props.onSetStudentCourseHomeworkId(courseId, courseLessonId, teacherCourseId);
                }
            } else if (type === PAGE_TYPE.initLogin) {
                const code = queryParams.code;
                this.props.onSetWeiXinCode(code);
            }
        }



        // 获取loginToken
        readLoginToken () {
            const loginToken = window.localStorage.getItem('h_token');
            log.info('是否登录loginToken', loginToken);
            // loginToken = 'ae674f7ec1ce09b124ed21471e9780c4';
            this.props.onSetLoginToken(loginToken);
            return loginToken;
        }

        // 获取登录信息
        loginInfo (loginToken){
            studentInfo(loginToken)
                .then(result => {
                    log.info('获取个人信息成功', result);
                    this.props.onSetUserInfo(result.data);
                })
                .catch(error => {
                    log.info(error);
                });
        }

        setActiveCards (tutorialId) {
            this.props.onUpdateReduxDeck(tutorialId);
        }
        openTutorials () {
            this.props.onOpenTipsLibrary();
        }
        render () {
            const {
                onOpenTipsLibrary, // eslint-disable-line no-unused-vars
                onUpdateReduxDeck, // eslint-disable-line no-unused-vars
                ...componentProps
            } = this.props;
            return (
                <WrappedComponent
                    {...componentProps}
                />
            );
        }
    }
    QueryParserComponent.propTypes = {
        onOpenTipsLibrary: PropTypes.func,
        onSetLoginToken: PropTypes.func,
        onSetPageType: PropTypes.func,
        onSetProductionId: PropTypes.func,
        onSetProjectUrl: PropTypes.func,
        onSetStudentCourseHomeworkId: PropTypes.func,
        onSetUserInfo: PropTypes.func,
        onSetWeiXinCode: PropTypes.func,
        onUpdateReduxDeck: PropTypes.func
    };
    const mapDispatchToProps = dispatch => ({
        onOpenTipsLibrary: () => {
            dispatch(openTipsLibrary());
        },
        onSetLoginToken: loginToken => dispatch(setLoginToken(loginToken)),
        onSetPageType: pageType => dispatch(setPageType(pageType)),
        onSetProductionId: productionId => dispatch(setProductionId(productionId)),
        onSetWeiXinCode: weiXinCode => dispatch(setWeiXinCode(weiXinCode)),
        onSetProjectUrl: url => dispatch(setProjectUrl(url)),
        onUpdateReduxDeck: tutorialId => {
            dispatch(activateDeck(tutorialId));
        },
        onSetPlayer: isShow => dispatch(setPlayer(isShow)),
        onSetUserInfo: userInfo => dispatch(saveUserInfo(userInfo)),
        onSetStudentCourseHomeworkId: (courseId, courseLessonId, teacherCourseId) => dispatch(
            setStudentCourseHomeworkId(courseId, courseLessonId, teacherCourseId))
    });
    return connect(
        null,
        mapDispatchToProps
    )(QueryParserComponent);
};

export {
    QueryParserHOC as default
};
