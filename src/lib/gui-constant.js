export const weixinLoginUrl = 'https://open.weixin.qq.com/connect/qrconnect?' +
    'appid=wx41ddc056b87016ba&redirect_uri=https://www.huazilive.com/scratch/?type=4&response_type=code&scope=snsapi_login#wechat_redirect';

export const loadType = {
    leaveUnused: 0,
    loadSuccess: 1,
    loadFail: 2
};

export const PRODUCTION_SELF = {
    no: 1,
    yes: 2
};

export const PAGE_TYPE = {
    showWork: 1,
    createHomeWork: 2,
    createProduction: 3,
    initLogin: 4
};

export const SOURCE_TYPE = {
    SYSTEM_TYPE: 1,
    TEACHER_TYPE: 2,
    STUDENT_TYPE: 3
};

export const GUIDANCE_TEXT = {
    oneText: '欢迎来到海码王！这里，就是你用作品闯荡的王国，你将会在这里遇见志同道合的小伙伴！下面就由我——海码王，带你熟悉一下Scratch3.0的操作界面吧！',
    towText: '你现在看到的，就是“舞台区”。所有的角色都将在这里，遵循着你的积木代码，在舞台上进行表演哦～点击左上角第三个图标，就可以全屏显示你的作品啦！',
    threeText: '那演员和背景从哪里来呢？自然就是眼前的“角色背景配置区”啦～在这里，你可以从角色/背景库中选择你的演员和舞台背景，同时还支持你上传自己的素材哦（咳咳，注意图片的版权归属哦～）',
    fourText: '这里就是你发挥神奇的魔力的区域啦——积木区。根据不同的颜色和功能提示，选择你要使用的积木块，就能让你的演员和背景作出不同的表演。',
    fiveText: '这里还空白着的就是脚本区。将彩色积木块拖拽到这里，拼接起来，演员才能真正开始连贯的表演哦。对了，拼完不要忘记点击舞台区绿色的旗帜按钮哦，那会是所有故事的开始……',
    sixText: '记得保存前给你的大作取个响亮的名字呀！创作过程中也记得常点保存按钮哦，不然辛苦可能就白费了(T ^ T)～当你完成整个作品之后，你就可以发布啦！接受众人的膜拜吧！',
    sevenText: '以上就是关于Scratch3.0的初步介绍，更有趣更好玩的技能就在创作中去解锁吧！祝你在海码王创作愉快呐！(*^__^*)'
};

export const baseUrl = 'http://192.168.0.128:3000';
