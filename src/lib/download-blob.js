import log from '../lib/log';
import ApiUtils from './ApiUtils';
import {SOURCE_TYPE} from './gui-constant';

export default (filename, blob) => {
    const downloadLink = document.createElement('a');
    document.body.appendChild(downloadLink);

    // Use special ms version if available to get it working on Edge.
    if (navigator.msSaveOrOpenBlob) {
        navigator.msSaveOrOpenBlob(blob, filename);
        return;
    }

    const url = window.URL.createObjectURL(blob);
    downloadLink.href = url;
    downloadLink.download = filename;
    downloadLink.click();
    window.URL.revokeObjectURL(url);
    document.body.removeChild(downloadLink);
};

// 保存文件
export const saveFile = function (loginToken, filename, blob, ...rest) {

    const fileData = new FormData();
    fileData.append('upload', blob, filename);
    fileData.append('loginToken', loginToken);

    return new Promise((resolve, reject) => {
        ApiUtils.post('/account/student/uploadFile', fileData)
            .then(result => {
                if (result.code === 200) {
                    console.log('上传文件成功', result, rest);
                    // 设置封面
                    result.data.coverUrl = rest ? rest[0] : '';
                    resolve(result);
                } else {
                    reject(result);
                }
            });
    });
};

// 真实的保存作品
const realSaveProduction = function (loginToken, name, content, iamgeUrl) {
    const fileData = new FormData();
    fileData.append('loginToken', loginToken);
    fileData.append('sourceType', SOURCE_TYPE.STUDENT_TYPE);
    fileData.append('name', name);
    fileData.append('content', content);
    fileData.append('category', 1);

    return new Promise((resolve, reject) => {
        ApiUtils.post('/account/student/production/save', fileData)
            .then(result => {
                if (result.code === 200) {
                    resolve(result.data);
                } else {
                    reject(result);
                }
            })
            .catch(error => {
                reject(error);
            });
    });
};

// 保存作品
export const saveProduction = function (loginToken, filename, fileblob, imageBlob, imageName, name) {
    return saveFile(loginToken, imageName, imageBlob)
        .then(result => saveFile(loginToken, filename, fileblob, result.data.url))
        .then(result => realSaveProduction(loginToken, name, result.data.url, result.data.coverUrl));
};


// 更新地址
export const changeURLPar = function (destiny, par, parValue) {
    const pattern = par + '=([^&]*)';
    const replaceText = par + '=' + parValue;
    if (destiny.match(pattern)) {
        let tmp = '/\\' + par + '=[^&]*/';
        tmp = destiny.replace(eval(tmp), replaceText);
        return (tmp);
    }
    if (destiny.match('[\?]')) {
        return destiny + '&' + replaceText;
    }
    return destiny + '?' + replaceText;
};
