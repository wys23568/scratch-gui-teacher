import React from 'react';
import PropTypes from 'prop-types';
import {intlShape, injectIntl} from 'react-intl';
import bindAll from 'lodash.bindall';
import {connect} from 'react-redux';
import {message} from 'antd';

import {setProjectUnchanged} from '../reducers/project-changed';
import {
    LoadingStates,
    getIsCreatingNew,
    getIsFetchingWithId,
    getIsLoading,
    getIsShowingProject,
    onFetchedProjectData,
    projectError,
    setProjectId, LoadingState
} from '../reducers/project-state';
import {
    activateTab,
    BLOCKS_TAB_INDEX
} from '../reducers/editor-tab';

import log from './log';
import storage from './storage';
import {PAGE_TYPE, PRODUCTION_SELF} from "./gui-constant";
import {homeworkDetail} from "./couresHomework";
import {productionDetail} from "./productionFetch";
import {setProductionId, setVideoUrl} from '../reducers/mode';

/* Higher Order Component to provide behavior for loading projects by id. If
 * there's no id, the default project is loaded.
 * @param {React.Component} WrappedComponent component to receive projectData prop
 * @returns {React.Component} component with project loading behavior
 */
const ProjectFetcherHOC = function (WrappedComponent) {
    class ProjectFetcherComponent extends React.Component {
        constructor (props) {
            super(props);
            bindAll(this, [
                'fetchProject',
            ]);
            storage.setProjectHost(props.projectHost);
            storage.setAssetHost(props.assetHost);
            storage.setTranslatorFunction(props.intl.formatMessage);
            // props.projectId might be unset, in which case we use our default;
            // or it may be set by an even higher HOC, and passed to us.
            // Either way, we now know what the initial projectId should be, so
            // set it in the redux store.
            if (
                props.projectId !== '' &&
                props.projectId !== null &&
                typeof props.projectId !== 'undefined'
            ) {
                this.props.setProjectId(props.projectId.toString());
            }
            this.fileReader = new FileReader();
            this.fileReader.onload = this.onLoad.bind(this);
        }
        componentDidUpdate (prevProps) {
            if (prevProps.projectHost !== this.props.projectHost) {
                storage.setProjectHost(this.props.projectHost);
            }
            if (prevProps.assetHost !== this.props.assetHost) {
                storage.setAssetHost(this.props.assetHost);
            }
            if (this.props.isFetchingWithId && !prevProps.isFetchingWithId) {
                // 展示作品
                if (this.props.pageType === PAGE_TYPE.showWork){
                    // 获取项目数据
                    this.loadProjectFile.bind(this, this.props.projectUrl)();
                } else if (this.props.pageType === PAGE_TYPE.createHomeWork && this.props.loginToken){
                    homeworkDetail(this.props.loginToken, this.props.courseId,
                        this.props.courseLessonId, this.props.teacherCourseId)
                        .then(result => {
                            // 获取作业作品url
                            const homeworkInfo = result.studentCourseHomeworkInfo;
                            const homeworkUrl = homeworkInfo.draft ? homeworkInfo.draft : homeworkInfo.content;
                            // 设置视屏地址
                            this.props.onSetVideoUrl(result.videoUrl);

                            if (result.name) {
                                // 设置项目名称
                                this.props.onUpdateProjectTitle(result.name);
                            }
                            if (homeworkUrl) {
                                this.loadProjectFile.bind(this, homeworkUrl)();
                            } else {
                                // 没有url加载原始项目
                                this.fetchProject(this.props.reduxProjectId, this.props.loadingState);
                            }
                        })
                        .catch(error => {
                            message.error(error.error);
                            // 没有url加载原始项目
                            this.fetchProject(this.props.reduxProjectId, this.props.loadingState);
                        });
                } else if (this.props.pageType === PAGE_TYPE.createProduction && this.props.productionId) {
                    // 获取作品信息
                    productionDetail(this.props.loginToken, this.props.productionId)
                        .then(result => {
                            // 不是自己的把Id设置为空
                            if (result.selfFlag === PRODUCTION_SELF.no) {
                                // 设置作品id为0
                                this.props.onSetProductionId(0);
                            }

                            if (result.content){
                                this.loadProjectFile.bind(this, result.content)();
                                const name = result.name ? result.name : '海马王作品';
                                this.props.onUpdateProjectTitle(name);
                            } else {
                                // 没有url加载原始项目
                                this.fetchProject(this.props.reduxProjectId, this.props.loadingState);
                                this.props.onUpdateProjectTitle('海马王作品');
                            }
                        })
                        .catch(error => {
                            message.error(error.error);
                        });
                } else {
                    this.fetchProject(this.props.reduxProjectId, this.props.loadingState);
                    this.props.onUpdateProjectTitle('海马王作品');
                }

            }
            if (this.props.isShowingProject && !prevProps.isShowingProject) {
                this.props.onProjectUnchanged();
            }
            if (this.props.isShowingProject && (prevProps.isLoadingProject || prevProps.isCreatingNew)) {
                this.props.onActivateTab(BLOCKS_TAB_INDEX);
            }
        }

        // 加载文件
        onLoad () {
            if (this.fileReader) {
                // 设置项目数据
                this.props.onFetchedProjectData(this.fileReader.result, this.props.loadingState);
            }
        }

        // 通过文件加载项目
        loadProjectFile (url) {
            fetch(url).then(res => res.blob().then(blob => {
                this.fileReader.readAsArrayBuffer(blob);
            }));
        }

        fetchProject (projectId, loadingState) {
            return storage
                .load(storage.AssetType.Project, projectId, storage.DataFormat.JSON)
                .then(projectAsset => {
                    if (projectAsset) {
                        this.props.onFetchedProjectData(projectAsset.data, loadingState);
                    } else {
                        // Treat failure to load as an error
                        // Throw to be caught by catch later on
                        throw new Error('Could not find project');
                    }
                })
                .catch(err => {
                    this.props.onError(err);
                    log.error(err);
                });
        }


        render () {
            const {
                /* eslint-disable no-unused-vars */
                assetHost,
                intl,
                isLoadingProject: isLoadingProjectProp,
                loadingState,
                loginToken,
                projectUrl,
                onActivateTab,
                onError: onErrorProp,
                onFetchedProjectData: onFetchedProjectDataProp,
                onProjectUnchanged,
                projectHost,
                projectId,
                courseId,
                courseLessonId,
                teacherCourseId,
                reduxProjectId,
                productionId,
                setProjectId: setProjectIdProp,
                /* eslint-enable no-unused-vars */
                isFetchingWithId: isFetchingWithIdProp,
                ...componentProps
            } = this.props;
            return (
                <WrappedComponent
                    fetchingProject={isFetchingWithIdProp}
                    {...componentProps}
                />
            );
        }
    }
    ProjectFetcherComponent.propTypes = {
        assetHost: PropTypes.string,
        canSave: PropTypes.bool,
        courseId: PropTypes.number,
        courseLessonId: PropTypes.number,
        intl: intlShape.isRequired,
        isFetchingWithId: PropTypes.bool,
        isLoadingProject: PropTypes.bool,
        loadingState: PropTypes.oneOf(LoadingStates),
        loginToken: PropTypes.string,
        onActivateTab: PropTypes.func,
        onError: PropTypes.func,
        onFetchedProjectData: PropTypes.func,
        onProjectUnchanged: PropTypes.func,
        onSetProductionId: PropTypes.func,
        onSetVideoUrl: PropTypes.func,
        onUpdateProjectTitle: PropTypes.func,
        pageType: PropTypes.number,
        projectHost: PropTypes.string,
        projectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        projectUrl: PropTypes.string,
        reduxProjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        setProjectId: PropTypes.func,
        teacherCourseId: PropTypes.number
    };
    ProjectFetcherComponent.defaultProps = {
        assetHost: 'https://assets.scratch.mit.edu',
        projectHost: 'https://projects.scratch.mit.edu'
    };

    const mapStateToProps = state => ({
        isCreatingNew: getIsCreatingNew(state.scratchGui.projectState.loadingState),
        isFetchingWithId: getIsFetchingWithId(state.scratchGui.projectState.loadingState),
        isLoadingProject: getIsLoading(state.scratchGui.projectState.loadingState),
        isShowingProject: getIsShowingProject(state.scratchGui.projectState.loadingState),
        loadingState: state.scratchGui.projectState.loadingState,
        loginToken: state.scratchGui.mode.loginToken,
        pageType: state.scratchGui.mode.pageType,
        projectUrl: state.scratchGui.mode.projectUrl,
        courseId: state.scratchGui.mode.courseId,
        courseLessonId: state.scratchGui.mode.courseLessonId,
        productionId: state.scratchGui.mode.productionId,
        teacherCourseId: state.scratchGui.mode.teacherCourseId,
        reduxProjectId: state.scratchGui.projectState.projectId
    });
    const mapDispatchToProps = dispatch => ({
        onActivateTab: tab => dispatch(activateTab(tab)),
        onError: error => dispatch(projectError(error)),
        onFetchedProjectData: (projectData, loadingState) =>
            dispatch(onFetchedProjectData(projectData, loadingState)),
        setProjectId: projectId => dispatch(setProjectId(projectId)),
        onSetVideoUrl: videoUrl => dispatch(setVideoUrl(videoUrl)),
        onProjectUnchanged: () => dispatch(setProjectUnchanged()),
        onSetProductionId: productionId => dispatch(setProductionId(productionId))
    });
    // Allow incoming props to override redux-provided props. Used to mock in tests.
    const mergeProps = (stateProps, dispatchProps, ownProps) => Object.assign(
        {}, stateProps, dispatchProps, ownProps
    );
    return injectIntl(connect(
        mapStateToProps,
        mapDispatchToProps,
        mergeProps
    )(ProjectFetcherComponent));
};

export {
    ProjectFetcherHOC as default
};
