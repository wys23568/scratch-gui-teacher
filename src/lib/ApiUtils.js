// 基础网络请求
import log from '../lib/log';
const testUrl = 'https://test.huazilive.com/api/haimawang';
const serviceTestUrl = 'https://test.huazilive.com/api/service';
// let testUrl = 'http://59.110.6.198/api/tiku';
// let testUrl = 'https://api.huazilive.com/api/tiku';
// import  fetch from 'whatwg-fetch'
// import Promise from 'es6-promise'

export default class ApiUtils {
    static get (url) {
        const tempUrl = testUrl;
        return new Promise((resolve, reject) => {
            fetch(tempUrl + url)
                .then(response => response.json())
                .then(result => {
                    resolve(result);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    static post (url, data) {
        const tempUrl = testUrl;
        log.info('url: ', tempUrl + url, '请求参数：', ...data);
        return new Promise((resolve, reject) => {
            fetch(tempUrl + url, {
                method: 'POST',
                header: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: data
            })
                .then(response => response.json())
                .then(result => {
                    resolve(result);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    static postService (url, data) {
        const tempUrl = serviceTestUrl;
        log.info('url: ', tempUrl + url, '请求参数：', ...data);
        return new Promise((resolve, reject) => {
            fetch(tempUrl + url, {
                method: 'POST',
                header: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: data
            })
                .then(response => response.json())
                .then(result => {
                    resolve(result);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }
}
