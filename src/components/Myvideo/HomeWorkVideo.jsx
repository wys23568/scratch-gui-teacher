import React from 'react';
import MyVideo from './Myvideo.js';
import './HomeWorkVideo.less';
import {Icon} from 'antd';
import bindAll from 'lodash.bindall';

export default class HomeWorkVideo extends React.Component {

    constructor (props) {
        super(props);
        this.state = {
            visible: false,
            width:'100%',
            height:'100%'
        };
        bindAll(this, ['handleHiddenVideo', 'handleShowVideo']);
    }

    handleHiddenVideo () {
        this.props.closeHomeWorkVideoModal();
        this.setState({
            width: '84px',
            height: '66px'
        });
    }
    handleShowVideo () {
        this.setState({
            width:'100%',
            height:'100%'
        });
        this.props.openHomeWorkVideoModal();
    }

    render () {
        const {width, height} = this.state;
        return (
            <div style={{position: 'relative', width: '100vw'}}>
                <div
                    className={'show-background'}
                    style={{
                        backgroundColor: this.props.homeworkVideoVisible ? 'rgba(0, 0, 0, 0.4)' : 'rgba(0, 0, 0, 0)',
                        width: width,
                        height: height,
                        right: this.props.homeworkVideoVisible ? 0 : '10px',
                        top: this.props.homeworkVideoVisible ? 0 : '160px'
                    }}
                >
                    <div
                        className={'show-video-content'}
                        onClick={e => {
                            e.stopPropagation();
                        }}
                    >
                        {this.props.homeworkVideoVisible ? <div
                            className={'show-close-div'}
                            onClick={this.handleHiddenVideo}
                        >
                            <span className={'show-follow-suit'}>
                                {'跟着手动'}
                            </span>
                            <Icon
                                className={'show-colse-icon'}
                                theme={'filled'}
                                type={'close-square'}
                                onClick={this.handleHiddenVideo}
                            />
                        </div> : null}

                        <MyVideo
                            height={this.props.homeworkVideoVisible ? '650px' : height}
                            homeworkVideoVisible={this.props.homeworkVideoVisible}
                            videoUrl={this.props.videoUrl}
                            width={this.props.homeworkVideoVisible ? '1068px' : width}
                        />

                        {
                            // 开始按钮
                            this.props.homeworkVideoVisible ? null : <div
                                style={{

                                    backgroundColor: 'rgba(0, 0, 0, 0.5)',
                                    height: height,
                                    width: width,
                                    position: 'absolute',
                                    display: 'flex',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    left: 0,
                                    top: 0}}
                                onClick={this.handleShowVideo}
                            >
                                <img
                                    src={require('../../assets/video_play.png')}
                                    style={{width: '22px', height: '22px'}}
                                />
                            </div>
                        }
                    </div>
                </div>
            </div>
        );
    }
}
