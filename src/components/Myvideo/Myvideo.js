import React, {useState, useEffect} from 'react';
import './Myvideo.less';
import {Row, Icon, Slider} from 'antd';

const Myvideo = React.memo(
    props => {
        const {videoUrl, width='1068px', height='650px', homeworkVideoVisible} = props
        const [media, setMedia] = useState(null)
        const [playbackProgress, setPlaybackProgress] = useState(0)
        const [play, setPlay] = useState(false)
        const [full, setFull] = useState(false)
        const [volume, setVolume] = useState(30)
        const [duration, setDuration] = useState(0)
        const [showVoice, setShowVoice] = useState(false)
        const [mute, setMute] = useState(false)
        const [showControls, setShowControls] = useState(false);

        useEffect(() => {
            document.getElementById("media").volume = 0.3;
            document.oncontextmenu = () => { return false };
            setVolume(30);
            return () => {
                document.oncontextmenu = () => { return true }
            }
        }, []);

        const playVideo = () => {
            if (play) {
                setPlay(false)
                media.pause()
                return
            }
            else {
                setPlay(true)
                media.play()
            }

        };

        useEffect(() => {
            if (!homeworkVideoVisible && play) {
                playVideo();
            }
        });

        const secondToDate = (val) => {
            var h = Math.floor(val / 3600) < 10 ? `0${Math.floor(val / 3600)}` : Math.floor(val / 3600),
                m = Math.floor((val / 60 % 60)) < 10 ? `0${Math.floor((val / 60 % 60))}` : Math.floor((val / 60 % 60)),
                s = Math.floor((val % 60)) < 10 ? `0${Math.floor((val % 60))}` : Math.floor((val % 60));
            return `${h}:${m}:${s}`
        }

        const onCanPlay = (e) => {
            setDuration(e.target.duration)
        }
        const onTimeUpdate = (e) => {
            setPlaybackProgress(e.target.currentTime)
        }
        const FullScreen = (ele) => {
            if (ele.requestFullscreen) {
                ele.requestFullscreen();
            } else if (ele.mozRequestFullScreen) {
                ele.mozRequestFullScreen();
            } else if (ele.webkitRequestFullScreen) {
                ele.webkitRequestFullScreen();
            }
        }
        const exitFullscreen = (de) => {
            if (de.exitFullscreen) {
                de.exitFullscreen();
            } else if (de.mozCancelFullScreen) {
                de.mozCancelFullScreen();
            } else if (de.webkitCancelFullScreen) {
                de.webkitCancelFullScreen();
            }
        }


        const changeScreen = () => {
            const userVideo = document.getElementById("user-video");
            document.onfullscreenchange = (e) => {
                if (document.fullscreenElement === null) {
                    setFull(false)
                }
            }
            if (full) {
                exitFullscreen(document)
                setFull(false)
                return
            }
            else {
                FullScreen(userVideo)
                setFull(true)
            }

        }
        const muteVolume = () => {
            if (mute) {
                setMute(false)
                return
            }
            setMute(true)
        }
        const changeVolume = (value) => {
            setVolume(value);
            media.volume = value / 100;
        }
        const changeCurrentTime = (value) => {
            setPlaybackProgress(value)
            media.currentTime = value
        }
        const onEnded = () => {
            setPlay(false);
        }


        return <div id='user-video'
                    style={{width:width,height: height}}
                    onMouseOver={() => (homeworkVideoVisible && setShowControls(true))}
                    onMouseLeave={() => {
                        homeworkVideoVisible && setShowControls(false)
                    }}>
            <video id='media'
                   preload='auto'
                   onCanPlay={onCanPlay}
                   onEnded={onEnded}
                   onClick={playVideo}
                   volume={volume / 100}
                   muted={mute}
                   onTimeUpdate={onTimeUpdate}
                   ref={el => setMedia(el)}
                   src={videoUrl || "https://vdse.bdstatic.com//b4468262ada0ce232a35770e703fda88?authorization=bce-auth-v1%2F40f207e648424f47b2e3dfbb1014b1a5%2F2017-05-11T09%3A02%3A31Z%2F-1%2F%2F24898a16598743be9189fcaa405cd84893da37a5378d5238235df378bd4098e3"}>
                您的浏览器不支持 video 标签。
            </video>
            {
                showControls && <Row className='row-position' type='flex' justify='start'>
                    <div className='video-row' type='flex' justify='start' align='middle'>
                        <div className='video-row-play-button' onClick={playVideo}>{play ? <Icon type="pause-circle"/> :
                            <Icon type="play-circle"/>}</div>
                        <div className='video-row-play-time'>{secondToDate(playbackProgress)}</div>
                        <Slider style={{flex: 1}} onChange={changeCurrentTime} value={playbackProgress}
                                max={parseInt(duration)} step={0.25}
                                tipFormatter={value => secondToDate(value)}
                        />
                        <div className='video-row-play-time-right'>{secondToDate(duration)}</div>
                    </div>
                    <div
                        onMouseOver={() => (setShowVoice(true))}
                        onMouseLeave={() => {
                            setShowVoice(false)
                        }}> {showVoice && <div className='volume'>
                        <Slider vertical onChange={changeVolume} value={volume} disabled={mute}
                        /></div>}
                        <div className='voice-img'><img
                            onClick={muteVolume}
                            style={{width: '23px', height: '23px'}}
                            // src={`${process.env.PUBLIC_URL}/assets/${mute ? 'icon-close-sound' : 'icon-open-sound'}.png`}
                            src={mute ? require('../../assets/icon-close-sound.png') : require('../../assets/icon-open-sound.png')}
                            alt=''/></div>

                    </div>

                    <div className='voice-img1'><img onClick={changeScreen}
                                                     style={{width: '23px', height: '23px', marginRight: '32px'}}
                                                     src={full ? require('../../assets/icon-exit-full-screen.png') : require('../../assets/icon-full-screen.png')}
                                                     alt=''/>
                    </div>
                </Row>
            }

        </div>
    }
);
export default Myvideo;
