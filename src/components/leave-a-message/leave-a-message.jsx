import React, {useState} from 'react';
import './leave-a-message.less';
import {connect} from 'react-redux';
import {Input, message} from 'antd';
import {messageCommit} from '../../lib/couresHomework';
const {TextArea} = Input;

const LeaveAMessage = React.memo(
    props => {
        const {loginToken, courseId, courseLessonId, teacherCourseId} = props;
        const [popupVisible, setPopupVisible] = useState(false);
        const [text, setText] = useState({count: 0, value: ''});

        const commit = () => {
            messageCommit(loginToken, courseId, courseLessonId, teacherCourseId, text.value)
                .then(result => {
                    message.success('提交成功');
                    setPopupVisible(false);
                    setText({count: 0, value: ''});
                })
                .catch(error => {
                    message.error('提交失败');
                });
        };

        return (
            <div>
                {popupVisible ? <div className={'leave-a-message-popup'}>
                    <div className={'head'}>
                        <span className={'title'}>
                            {'我的疑问'}
                        </span>
                        <img
                            className={'close'}
                            src={require('../../assets/close_icon.png')}
                            onClick={() => {
                                setPopupVisible(false);
                                setText({count: 0, value: ''});
                            }}
                        />
                    </div>
                    <div
                        className={'input-root'}
                    >
                        <TextArea
                            allowClear={0}
                            className={'content'}
                            disabled={false}
                            maxLength={500}
                            placeholder={'可以将问题按照\n' +
                            '1、看不清看不懂整不明白；\n' +
                            '2、看不清看不懂整不明白；\n' +
                            '3、看不清看不懂整不明白；\n' +
                            '罗列出来，这样表达更清晰哦～'}
                            type={Input.TextArea}
                            onChange={e => {
                                setText({count: e.target.value.length, value: e.target.value});
                            }}
                        />
                        <span className={'text-change'}>
                            {`${text.count}/500`}
                        </span>
                    </div>
                    <div className={'explain-root'}>
                        <span className={'explain-text'}>
                            {'提交后班主任将很快给予解答哦'}
                        </span>
                        <img
                            className={'explain-img'}
                            src={require('../../assets/explain_icon.png')}
                        />
                    </div>
                    <div
                        className={'button'}
                        onClick={() => {
                            if (text.value){
                                commit();
                            }
                        }}
                    >
                        {'提交'}
                    </div>
                </div> : null}
                <div
                    onClick={() => {
                        setPopupVisible(true);
                    }}
                    className={'leave-a-message'}
                >
                    <img
                        className={'img'}
                        src={require('../../assets/leave_a_message_icon.png')}
                    />
                    <span className={'text'}>
                        {'给老师留言'}
                    </span>
                </div>
            </div>
        );
    }
);

export default LeaveAMessage;
