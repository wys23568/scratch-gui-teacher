import React from 'react';
import bindAll from 'lodash.bindall';
import './Loading.less';
import {Spin} from 'antd';

class LoadingText extends React.Component {
    constructor (props) {
        super(props);

        bindAll(this, ['handleOnClick']);
    }

    handleOnClick (e) {
        e.stopPropagation();
    }

    render () {
        return (
            <div
                className={'laoding_root'}
                onClick={this.handleOnClick}
            >
                <div className={'content'}>
                    <Spin />
                    <span className={'text'}>
                        {this.props.content}
                    </span>
                </div>
            </div>);
    }
}

export default LoadingText;
