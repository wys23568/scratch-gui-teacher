import React, {useState} from 'react';
import {GUIDANCE_TEXT} from '../../lib/gui-constant';
import './Guidance.less';


const Guidance = React.memo(
    props => {
        const {
            closeGuidance,
            borderHeight,
            borderWidthValue,
            stageDimensions
        } = props;
        const [text, setText] = useState(GUIDANCE_TEXT.oneText);
        let [step, setStep] = useState(0);
        const [contentStyleClass, setContentStyle] = useState('scratch-guide-other');
        const [border, setBorder] = useState({
            top: '0',
            right: `${borderWidthValue}px`,
            bottom: `${borderHeight}px`,
            left: '0px',
            width: 0,
            height: 0
        });
        const [contentMargin, setContentMargin] = useState({
            top: 'calc(50vh - 125px)',
            left: 'calc(50vw - 213px)'
        });
        const oneAndSevenStep = () => {
            if (step === 0) {
                setText(GUIDANCE_TEXT.oneText);
            } else {
                setText(GUIDANCE_TEXT.sevenText);
            }

            // 获取可见页面的宽高
            const h = document.documentElement.clientHeight;
            const w = document.documentElement.clientWidth;
            setBorder({top: `0`, right: `${w}Px`, bottom: `${h}Px`, left: '0'});
            setContentMargin({
                top: 'calc(50vh - 125px)',
                left: 'calc(50vw - 213px)'
            });
        };

        const towStep = () => {
            setText(GUIDANCE_TEXT.towText);
            setBorder({
                top: `3rem`,
                right: `calc(100vw - ${stageDimensions.width}px - 1rem)`,
                bottom: `calc(100vh - ${stageDimensions.height}px - 6.25rem)`,
                left: '0',
                height: `calc(${stageDimensions.height}px + 3.25rem)`,
                width: `calc(${stageDimensions.width}px + 1rem)`
            });
            setContentMargin({
                top: '5.75rem',
                left: `calc(${stageDimensions.width}px + 5rem)`
            });
        };

        const threeStep = () => {
            setText(GUIDANCE_TEXT.threeText);
            setBorder({
                top: `calc(${stageDimensions.height}px + 6.25rem)`,
                right: `calc(100vw - ${stageDimensions.width}px - 1rem)`,
                bottom: `0`,
                left: '0',
                height: `calc(100vh - ${stageDimensions.height}px - 6.25rem)`,
                width: `calc(${stageDimensions.width}px + 1rem)`
            });
            setContentMargin({
                top: `calc(${stageDimensions.height}px + 12.25rem)`,
                left: `calc(${stageDimensions.width}px + 5rem)`
            });
        };

        const fourStep = () => {
            setText(GUIDANCE_TEXT.fourText);
            setBorder({
                top: `3rem`,
                right: `calc(100vw - ${stageDimensions.width}px - 1rem - 310px)`,
                bottom: `0`,
                left: `calc(${stageDimensions.width}px + 0.5rem)`,
                height: `calc(100vh - 3rem)`,
                width: `calc(${250 + 60}px)`
            });
            setContentMargin({
                top: 'calc(50vh - 125px)',
                left: `calc(${stageDimensions.width}px + 5rem + 310px)`
            });
        };

        const fiveStep = () => {
            setText(GUIDANCE_TEXT.fiveText);
            setBorder({
                top: `3rem`,
                right: `0`,
                bottom: `0`,
                left: `calc(${stageDimensions.width}px + 1rem + 310px)`,
                height: `calc(100vh - 3rem)`,
                width: `calc(100vw - ${stageDimensions.width}px - 1rem - 310px)`
            });
            setContentMargin({
                top: 'calc(50vh - 125px)',
                left: `calc(${stageDimensions.width / 2}px)`
            });
        };

        const sixStep = () => {
            setText(GUIDANCE_TEXT.sixText);
            setBorder({
                top: `0`,
                right: `calc(100vw - 40rem)`,
                bottom: `calc(100vh - 3rem)`,
                left: `16rem`,
                height: `3rem`,
                width: `24rem`
            });
            setContentMargin({
                top: '129px',
                left: `calc(${stageDimensions.width + 260}px)`
            });
        };

        const changeText = () => {
            if (step === 0){
                oneAndSevenStep();
            } else if (step === 1) {
                towStep();
            } else if (step === 2) {
                threeStep();
            } else if (step === 3) {
                fourStep();
            } else if (step === 4) {
                fiveStep();
            } else if (step === 5) {
                sixStep();
            } else if (step === 6) {
                oneAndSevenStep();
            }
        };

        window.onresize = function () {
            changeText();
        };

        const lastStep = () => {
            if (step > 0) {
                setStep(--step);
                changeText();
            }

        };

        const nextStep = () => {
            if (step < 6){
                setStep(++step);
                changeText();
            }
        };

        return (
            <div className={'ui-scratch-guide'}>
                <div
                    className={'scratch-guide-cover scratch-cover-not-first'}
                    style={{
                        borderWidth: `${border.top} ${border.right} ${border.bottom} ${border.left}`,
                        width: border.width,
                        height: border.height
                    }}
                />
                <div
                    className={`scratch-guide-content ${contentStyleClass}`}
                    style={{top: contentMargin.top, left: contentMargin.left}}
                >
                    <div
                        className={'close-div'}
                    >
                        <img
                            src={require('../../assets/guidance_close.png')}
                            style={{width: '40px', height: '40px', paddingTop: '18px', paddingRight: '18px'}}
                            onClick={closeGuidance}
                        />
                    </div>
                    <span className={'content-text'}>
                        {text}
                    </span>
                    <div className={'last-and-next'}>
                        {step === 0 ? null : <span
                            className={'button button-right'}
                            onClick={lastStep}
                        >
                            {'上一步'}
                        </span>}
                        <span
                            className={'button'}
                            onClick={step === 6 ? closeGuidance : nextStep}
                        >
                            {step === 6 ? '完成' : '下一步'}
                        </span>
                    </div>
                    <div className="cartoon-box">
                        <img
                            alt="cartoon"
                            className="cartoon"
                            src={require('../../assets/guidance_right.png')}
                        />
                        <img
                            alt="cartoon-left"
                            className="cartoon-left"
                            src={require('../../assets/guidance_left.png')}
                        />
                    </div>
                </div>
            </div>
        );
    }
);

export default Guidance;
