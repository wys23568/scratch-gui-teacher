import React from 'react';
import {weixinLoginUrl} from '../../lib/gui-constant';
import './WeixinLogin.less';

class WeixinLogin extends React.Component {
    constructor (prpps) {
        super(prpps);
        this.state = {};
    }
    render () {
        return (<div
            className={'weixing-login'}
            onClick={this.props.handleHidden}
        >
            <iframe
                className={'iframe-style'}
                frameBorder={0}
                height={512}
                src={weixinLoginUrl}
                width={534}
                onclick={e => {
                    e.stopPropagation();
                }}
            />
        </div>);
    }
}


export default WeixinLogin;
