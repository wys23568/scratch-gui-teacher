import React from 'react';
import {loadType} from '../../lib/gui-constant';
import {Spin} from 'antd';
import {connect} from 'react-redux';
import './InitLogin.less';
import {weixinLogin} from './loginFetch.jsx';
import BindPhone from '../bindPhone/index.jsx';

class InitLogin extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            load: loadType.leaveUnused
        };
    }

    componentWillMount () {
        const appId = '1106266783';
        const appKey = '2421951481';
        const accountType = 'common';
        weixinLogin(appId, appKey, accountType, this.props.code)
            .then(result => {
                console.log('微信登录信息：', result);
                // history.replaceState({}, null, 'http://192.168.0.107:8601/?type=3&productionId=19');
                window.location.href = 'http://192.168.0.107:8601/?type=3&productionId=19';
                localStorage.setItem('h_token', 'ae674f7ec1ce09b124ed21471e9780c4');
                // this.setState({
                //     load: loadType.loadSuccess
                // });
            })
            .catch(error => {

            });
    }

    render() {
        return (
            this.state.load === loadType.leaveUnused ?
                <div className={'init-login'}>
                    <Spin />
                </div> : <BindPhone />
        );
    }
}

const mapStateToProps = state => ({
    code: state.scratchGui.mode.weiXinCode
});

export default connect(mapStateToProps)(InitLogin);
