import ApiUtils from '../../lib/ApiUtils';

export const weixinLogin = (appId, appKey, accountType, code) => {
    const formData = new FormData();
    formData.append('appId', appId);
    formData.append('appKey', appKey);
    formData.append('accountType', accountType);
    formData.append('code', code);
    return new Promise((resolve, reject) => {
        ApiUtils.post('/account/common/sso/weixin/login', formData)
            .then(result => {
                // if (result.code === 200) {
                //     resolve(result.data);
                // } else {
                //     reject(result);
                // }
                resolve(result);
            })
            .catch(error => {
                reject(error);
            });
    });
};

// 第三方登录-绑定手机获取验证码
export const thirdPartyGetCode = (ssoId, oauthId, verifyPhone) => {
    const formData = new FormData();
    formData.append('ssoId', ssoId);
    formData.append('oauthId', oauthId);
    formData.append('verifyPhone', verifyPhone);
    return new Promise((resolve, reject) => {
        ApiUtils.post('/account/common/sso/binding/request', formData)
            .then(result => {
                if (result.code === 200) {
                    resolve(result.data);
                } else {
                    reject(result);
                }
            })
            .catch(error => {
                reject(error);
            });
    });
};
// 第三方登录-绑定手机获取验证码
export const thirdPartyBindphone = (ssoId, oauthId, verifyPhone, verifyCode) => {
    const formData = new FormData();
    formData.append('ssoId', ssoId);
    formData.append('oauthId', oauthId);
    formData.append('verifyPhone', verifyPhone);
    formData.append('verifyCode', verifyCode);
    return new Promise((resolve, reject) => {
        ApiUtils.post('/account/common/sso/binding/confirm', formData)
            .then(result => {
                if (result.code === 200) {
                    resolve(result.data);
                } else {
                    reject(result);
                }
            })
            .catch(error => {
                reject(error);
            });
    });
};
