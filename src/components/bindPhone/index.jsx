/**
 * 绑定手机号
 * */
import React from 'react';
import './index.less';
import {Input, Button, Checkbox, message} from 'antd';
import {thirdPartyGetCode, thirdPartyBindphone} from '../weixin-login/loginFetch.jsx';


class BindPhone extends React.Component {
    // 构造
    constructor (props) {
        super(props);
        // 初始状态
        this.state = {
            checked: true,
            verificationBtnDisabled: false,
            time: 60
        };
    }


    componentWillUnmount() {
        clearTimeout(this.timer);
    }

    inputChange (e) {
        this.setState({
            phoneInput: e.target.value
        });
    }

    render() {
        return (
            <div className='bind-root'>
                <div className='bind-bg'>
                    <div className='header'>
                        <img
                            className='logo-img'
                            src={require('../../assets/logo.png')}
                        />
                        <span className='logo-title'>海码王</span>
                    </div>
                    <div className='body'>
                        <div className='bing-title'>请绑定用于接收课程消息通知的手机号</div>
                        <Input
                            className='input-number'
                            maxLength={11}
                            allowClear
                            value={this.state.phoneInput}
                            placeholder="请输入手机号"
                            onChange={this.inputChange.bind(this)}
                        />
                        <div className='verificvation-code-bg'>
                            <Input
                                className='input-code'
                                allowClear
                                placeholder="请输入验证码"
                                value={this.state.codeInput}
                                onChange={this.codeInput.bind(this)}
                            />
                            <Button
                                type="primary"
                                className="verificvation-code"
                                onClick={this.verificationCode.bind(this)}
                                disabled={this.state.verificationBtnDisabled}
                            >
                                {!this.state.verificationBtnDisabled ? '获取验证码' : `${this.state.time}s`}
                            </Button>
                        </div>
                        <div className='user-agreement-bg'>
                            <Checkbox
                                checked={this.state.checked}
                                onChange={(e) => {
                                    console.log('Checkbox ', e.target.checked);
                                    this.setState({
                                        checked: e.target.checked,
                                    })
                                }}/>
                            <span>{'我已阅读并同意'}
                                <span className='agreement' onClick={this.userAgreement}>
                                    {'《用户协议》'}
                                </span>
                            </span>
                        </div>
                        <div className='bind-phone-bg'>
                            <Button
                                type="primary"
                                disabled={!this.state.checked}
                                className="bind-phone"
                                onClick={this.bindPhone.bind(this)}
                            >
                                {'登陆'}
                            </Button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    codeInput (e) {
        console.log('codeInput', e.target.value);
        this.setState({
            codeInput: e.target.value
        });
    }
    verificationCode () {
        console.log('获取验证码', this.state.phoneInput);
        if (!this.state.phoneInput || this.state.phoneInput.length !== 11) {
            message.warning('请输入正确手机号');
            return;
        }
        this.setState({verificationBtnDisabled: true});
        this.timer = setInterval(() => {
            const {time} = this.state;
            this.setState({time: time - 1, verificationBtnDisabled: true},
                () => {
                    if (time === 1) {
                        clearInterval(this.timer);
                        this.setState({
                            time: 60,
                            verificationBtnDisabled: false
                        });
                    }
                });
        }, 1000);
        //:ssoId/:oauthId
        console.log('BindPhone ', this.props.match.params.ssoId, this.props.match.params.oauthId);
        const ssoId = this.props.match.params.ssoId;
        const oauthId = this.props.match.params.oauthId;
        thirdPartyGetCode(ssoId, oauthId, this.state.phoneInput).then((result) => {

            console.log('thirdPartyGetCode', result.data);

        }).catch((error) => {

        })

    }
    userAgreement (e) {
        console.log('用户协议', e);

    }
    bindPhone () {
        console.log('绑定手机');
        if (!this.state.phoneInput || this.state.phoneInput.length !== 11) {
            message.warning('请输入正确手机号');
            return;
        }
        if (!this.state.codeInput) {
            message.warning('请输入验证码');
            return;
        }

        const ssoId = this.props.match.params.ssoId;
        const oauthId = this.props.match.params.oauthId;
        thirdPartyBindphone(ssoId, oauthId, this.state.phoneInput, this.state.codeInput)
            .then(result => {
                console.log('thirdPartyBindphone', result.data);
                let loginToken = 'ae674f7ec1ce09b124ed21471e9780c4';
                localStorage.setItem("h_token", loginToken); //红涛

            })
            .catch(error => {

            });

    }

}

export default BindPhone;
