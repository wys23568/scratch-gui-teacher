const SET_FULL_SCREEN = 'scratch-gui/mode/SET_FULL_SCREEN';
const SET_PLAYER = 'scratch-gui/mode/SET_PLAYER';
const SET_PAGE_TYPE = 'scratch-gui/mode/SET_PAGE_TYPE';
const SET_LOGIN_TOKEN = 'scratch-gui/mode/SET_LOGIN_TOKEN';
const SET_STUDENT_COURSE_HOMEWORK_ID = 'scratch-gui/mode/SET_STUDENT_COURSE_HOMEWORK_ID';
const SET_USER_INFO = 'scratch-gui/mode/SET_USER_INFO';
const SET_PRODUCTION_ID = 'scratch-gui/mode/SET_PRODUCTION_ID';
const SET_PROJECT_URL = 'scratch-gui/mode/SET_PROJECT_URL';
const SET_VIDEO_URL = 'scratch-gui/mode/SET_VIDEO_URL';
const SET_WEI_XIN_CODE = 'scratch-gui/mode/SET_WEI_XIN_CODE';

const initialState = {
    showBranding: false,
    isFullScreen: false,
    isPlayerOnly: false,
    hasEverEnteredEditor: true,
    pageType: 0,
    loginToken: null,
    courseId: 0,
    courseLessonId: 0,
    teacherCourseId: 0,
    productionId: 0,
    videoUrl: null,
    userInfo: null,
    projectUrl: null,
    weiXinCode: null
};

const reducer = function (state, action) {
    if (typeof state === 'undefined') state = initialState;
    switch (action.type) {
    case SET_WEI_XIN_CODE:
        return Object.assign({}, state, {
            weiXinCode: action.weiXinCode
        });
    case SET_PRODUCTION_ID:
        return Object.assign({}, state, {
            productionId: action.productionId
        });
    case SET_VIDEO_URL:
        return Object.assign({}, state, {
            videoUrl: action.videoUrl
        });
    case SET_PROJECT_URL:
        return Object.assign({}, state, {
            projectUrl: action.projectUrl
        });

    case SET_USER_INFO:
        return Object.assign({}, state, {
            userInfo: action.userInfo
        });
    case SET_STUDENT_COURSE_HOMEWORK_ID:
        return Object.assign({}, state, {
            courseId: action.courseId,
            courseLessonId: action.courseLessonId,
            teacherCourseId: action.teacherCourseId
        });
    case SET_LOGIN_TOKEN:
        return Object.assign({}, state, {
            loginToken: action.loginToken
        });
    case SET_PAGE_TYPE:
        return Object.assign({}, state, {
            pageType: action.pageType
        });
    case SET_FULL_SCREEN:
        return Object.assign({}, state, {
            isFullScreen: action.isFullScreen
        });
    case SET_PLAYER:
        return Object.assign({}, state, {
            isPlayerOnly: action.isPlayerOnly,
            hasEverEnteredEditor: state.hasEverEnteredEditor || !action.isPlayerOnly
        });
    default:
        return state;
    }
};


const setWeiXinCode = function (weiXinCode) {
    return {
        type: SET_WEI_XIN_CODE,
        weiXinCode: weiXinCode
    };
};

const setProductionId = function (productionId) {
    return {
        type: SET_PRODUCTION_ID,
        productionId: productionId
    };
};

const setFullScreen = function (isFullScreen) {
    return {
        type: SET_FULL_SCREEN,
        isFullScreen: isFullScreen
    };
};

const setUserInfo = function (userInfo) {
    return {
        type: SET_USER_INFO,
        userInfo: userInfo
    };
};

const setProjectUrl = function (projectUrl) {
    return {
        type: SET_PROJECT_URL,
        projectUrl: projectUrl
    };
};

const setVideoUrl = function (videoUrl) {
    return {
        type: SET_VIDEO_URL,
        videoUrl: videoUrl
    };
};

const setLoginToken = function (loginToken) {
    return {
        type: SET_LOGIN_TOKEN,
        loginToken: loginToken
    };
};

const setPlayer = function (isPlayerOnly) {
    return {
        type: SET_PLAYER,
        isPlayerOnly: isPlayerOnly
    };
};

const setPageType = function (pageType) {
    return {
        type: SET_PAGE_TYPE,
        pageType: pageType
    };
};

const setStudentCourseHomeworkId = function (courseId, courseLessonId, teacherCourseId) {
    return {
        type: SET_STUDENT_COURSE_HOMEWORK_ID,
        courseId: courseId,
        courseLessonId: courseLessonId,
        teacherCourseId: teacherCourseId
    };
};


export {
    reducer as default,
    initialState as modeInitialState,
    setFullScreen,
    setLoginToken,
    setPageType,
    setPlayer,
    setProjectUrl,
    setVideoUrl,
    setProductionId,
    setStudentCourseHomeworkId,
    setUserInfo,
    setWeiXinCode
};
